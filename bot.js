var request = require('request');
var leetspeak = require('leetspeak');


const Discord = require('discord.js');
const client = new Discord.Client();
const apiUrl = "http://localhost:3000/";

var cat;
var difficulty;
var question;
var correct_answer;
var incorrect_answers;
var type;
var anwsers = new Array();

client.login('');

client.on('ready', () => {
    console.log("Hello FAM, I'm RepBot 2.0. Cash Me Outside Howbow Dah!");
});

client.on("message", (message) => {

    if(message.author.bot) return;

    // Checks for PREFIX
    if (message.content[0] === "!") {
        const command = message.content.toLowerCase().split(' ')[0].substring(1);
        const suffix = message.content.substring(command.length + 2);

        switch (command) {
            case "+rep": // Add rep
                repManagment(message, suffix, "add");
                break;
            case "-rep": // Substract rep
                repManagment(message, suffix, "subtract");
                break;
            case "pool": // Show DailyRepPool
                showDailyRepPool(message);
                break;
            case "gamble": // Gamble Reps
                playGame(message, suffix);
                break;
            case "stats": // Show leaderboard
                showLeaderboard(message);
                break;
            case "leet": // Show text in leatspeak
                leet(message, suffix);
                break;
            case "repbot?": // Does what it says
                repbot(message, suffix);
                break;
            case "trivia": // Play game of trivia
                trivia(message);
                break;
            case "answer": // Answer to a trivia question
                triviaAnswer(message, suffix);
                break;
        }
    } else if (message.content.substr(-1) === "?") {
        askFarla(message);
    }
});

function sendMessage(obj, textmessage) {
    obj.channel.sendEmbed({
        color: 3447003,
        description: textmessage
    });
}

function sendMessageTTS(obj, textmessage) {
    obj.channel.send(textmessage, {
        tts: true
    });
}

function askFarla(message) {
    var number = Math.floor(Math.random() * 7) + 1;

    // Only trigger once in a while
    if (number == 1) {
        let author = message.author.username;
        sendMessageTTS(message, author + ", ask Farla maybebaby?");
    }
}

function checkDailyPool() {
    request(apiUrl + 'repPool/', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            jsonData = JSON.parse(body);

            for (var i = 0; i < jsonData.data.length; i++) {
                var repData = jsonData.data[i];
                var id = repData.id;
                var repsPool = repData.reps;
            }

            if (repsPool > 0 && (repsPool - 1) >= 0) {
                repsPool = repsPool - 1;
                request.put(apiUrl + 'repPool/').form({
                    "id": id,
                    "reps": repsPool
                });

                return true;
            } else {
                return false;
            }
        }
        return false;
    });
}

function repManagment(message, suffix, action) {
    let author = message.author.username;

    if (suffix == "") {
        sendMessage(message, author + ", you think I'm GOD or something? Enter a goddamn name you PLEB!");
        return false;
    }

    let userid = suffix.replace(/[|&;$%@"<>()+!,]/g, "");
    let userObj = message.mentions.users.find(m => m.id === userid
)
    ;
    let user = userObj.username;

    // Check if guild data is present
    if (message.guild) {
        // Check if given username exists on server
        if (message.guild.members.find(m => m.user.username === user))
        {
            if (author != user) {
                try {
                    request(apiUrl + 'users/' + user, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            userData = JSON.parse(body);

                            for (var i = 0; i < userData.data.length; i++) {
                                // old
                                var userdata = userData.data[i];
                                // var reps = userdata.reps +1;

                                if (action == "add") {
                                    var reps = userdata.reps + 1;
                                } else if (action == "substract") {
                                    var reps = userdata.reps - 1;
                                }
                            }

                            request.put(apiUrl + 'users/').form({
                                "name": user,
                                "reps": reps
                            });
                        } else if (!error && response.statusCode == 404) {
                            // Users doesn't exist yet.
                            request.post(apiUrl + 'users/').form({
                                "name": user,
                                "reps": -1
                            });
                        }
                    });

                    request(apiUrl + 'users/' + author, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            jsonData = JSON.parse(body);

                            for (var i = 0; i < jsonData.data.length; i++) {
                                var userdata = jsonData.data[i];
                                var reps = userdata.reps;
                            }

                            if (checkDailyPool() == false) {
                                // No reps left for today
                                request.put(apiUrl + 'users/').form({
                                    "name": author,
                                    "reps": reps - 1
                                });
                            }
                        } else if (!error && response.statusCode == 404) {
                            if (checkDailyPool() == false) {
                                // No reps left for today
                                request.put(apiUrl + 'users/').form({
                                    "name": author,
                                    "reps": -1
                                });
                            } else {
                                request.put(apiUrl + 'users/').form({
                                    "name": author,
                                    "reps": 0
                                });
                            }
                        }
                    });

                    var notification = (action == "add" ? " sent a rep to " : " subtracted a rep from ");
                    sendMessage(message, author + notification + user + "'s account!");
                } catch (err) {
                    sendMessage(message, author + ", something fucked up happened! Like to your mom yesterday!");
                }
            } else {
                var notification = (action == "add" ? "add" : "subtract");
                sendMessage(message, "You can't " + notification + " reps to your own account " + author + ". WHAT THE F*@%$ PLEB?");
            }
        }
    else
        {
            sendMessage(message, user + " is'nt a valid user in this server. HOW BOW DAH?");
        }
    }
}

function showDailyRepPool(message) {
    request(apiUrl + 'repPool/', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var textmessage = "!========== [ Daily Rep Pool ] ==========!\n";

            jsonData = JSON.parse(body);

            for (var i = 0; i < jsonData.data.length; i++) {
                var repData = jsonData.data[i];
                var id = repData.id;
                var repsPool = repData.reps;
            }

            var textmessage = textmessage + "* Reps left for today: " + repsPool + "\n";
            textmessage = textmessage + "!================!===============!";
            sendMessage(message, textmessage);

        } else if (!error && response.statusCode == 404) {
            sendMessage(message, "Something fucked happend! Like to your mom yesterday!");
        }
    });
}

function showLeaderboard(message) {
    request(apiUrl + 'users/', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            usersData = JSON.parse(body);

            var textmessage = "!========== [ Leaderboard ] ==========!\n";

            for (var i = 0; i < usersData.data.length; i++) {
                var userdata = usersData.data[i];
                var name = userdata.name;
                var reps = userdata.reps;
                textmessage = textmessage + "* User: " + name + " Rep: " + reps + "\n";
            }

            textmessage = textmessage + "!================!===============!";
            sendMessage(message, textmessage);

        } else if (usersData != false && usersData == 404) {
            sendMessage(message, "Something fucked happened! Like to your mom yesterday!");
        }
    });
}

function playGame(message, bet) {
    let author = message.author.username;

    try {
        request(apiUrl + 'users/' + author, function (error, response, body) {
            if (!error && response.statusCode == 200) {

                jsonData = JSON.parse(body);

                for (var i = 0; i < jsonData.data.length; i++) {
                    var reps = jsonData.data[i].reps;
                }

                if ((reps - bet) >= 0) {
                    if (bet > 0) {

                        var game = gamble(reps, bet);

                        request.put(apiUrl + 'users/').form({
                            "name": author,
                            "reps": game[0].prizeMoney
                        });

                        var textmessage = "!========== [ SlotMachine ] ==========!\n";
                        textmessage = textmessage + "* " + author + " gambled " + bet + " reps.\n";
                        textmessage = textmessage + "* " + author + " got :" + game[0].slotA + ": :" + game[0].slotB + ": :" + game[0].slotC + ":\n";
                        textmessage = textmessage + "* " + author + " won " + game[0].prizeMoney + " reps.\n";
                        textmessage = textmessage + "!================!===============!";

                        sendMessage(message, textmessage);
                    } else {
                        sendMessage(message, author + ", your bet amount has to be more than 0 you MORON!");
                    }
                } else {
                    sendMessage(message, author + ", you need more reps to be able to bet!");
                }
            } else if (jsonData != false && jsonData == 404) {
                sendMessage(message, author + ", you can't bet yet!");
            }
        });
    } catch (err) {
        sendMessage(message, author + ", something fucked happened! Like to your mom yesterday!");
    } finally {
        sendMessage(message, "Thanks for gambling " + author + ". See you next time FAM!");
    }
}

function gamble(bank, bet) {

    var cards = ['diamonds', 'spades', 'hearts', 'clubs'];

    var a = numberChanger(Math.floor(Math.random() * 4) + 1);
    var b = numberChanger(Math.floor(Math.random() * 4) + 1);
    var c = numberChanger(Math.floor(Math.random() * 4) + 1);
    var prize = 0;

    if (a == 'hearts' && b == 'hearts' && c == 'hearts') {
        prize = ((bank - bet) + (bet * 30));
    } else if (a == 'diamonds' && b == 'diamonds' && c == 'diamonds') {
        prize = ((bank - bet) + (bet * 20));
    } else if (a == 'spades' && b == 'spades' && c == 'spades') {
        prize = ((bank - bet) + (bet * 10));
    } else if (a == 'clubs' && b == 'clubs' && c == 'clubs') {
        prize = ((bank - bet) + (bet * 4));
    } else if (a == 'clubs' && b == 'clubs') {
        prize = ((bank - bet) + (bet * 2));
    } else if (a == 'clubs' && c == 'clubs') {
        prize = ((bank - bet) + (bet * 2));
    } else if (b == 'clubs' && c == 'clubs') {
        prize = ((bank - bet) + (bet * 2));
    } else if (a == 'clubs' || b == 'clubs' || c == 'clubs') {
        prize = ((bank - bet) + (bet * 1));
    } else {
        prize = ((bank - bet));
    }

    var slot = [{
        prizeMoney: prize,
        slotA: a,
        slotB: b,
        slotC: c,
        bet: bet,
        bank: bank
    }];
    return slot;
}

function numberChanger(Number) {
    if (Number == 1) {
        return 'hearts';
    } else if (Number == 2) {
        return 'diamonds';
    } else if (Number == 3) {
        return 'spades';
    } else if (Number == 4) {
        return 'clubs';
    }
}

function leet(message, suffix) {
    sendMessage(message, leetspeak(suffix));
}

function trivia(message) {
    request('https://opentdb.com/api.php?amount=1', function (error, response, body) {
        jsonData = JSON.parse(body);

        cat = jsonData.results[0].category;
        difficulty = jsonData.results[0].difficulty;
        question = jsonData.results[0].question;
        correct_answer = jsonData.results[0].correct_answer;
        incorrect_answers = jsonData.results[0].incorrect_answers;
        type = jsonData.results[0].type;

        // Clear array
        anwsers = [];

        incorrect_answers.forEach(function (element) {
            anwsers.push(element);
        }, this);

        anwsers.push(correct_answer);

        // Only shuffle when it is mulitple choice
        if (type != 'boolean') {
            shuffle(anwsers);
        }

        var textmessage = "!========== [ Trivia Crackie ] ==========!\n";
        textmessage = textmessage + "*  Category: \n";
        textmessage = textmessage + "** " + cat.replace("&amp;quot;", "'") + " **\n\n";
        textmessage = textmessage + "*  Question: \n";
        textmessage = textmessage + "** " + question.replace("&amp;quot;", "'") + " **\n\n";
        textmessage = textmessage + "*  Possible answers:\n";

        for (var i = 0; i < anwsers.length; i++) {
            textmessage = textmessage + "** [" + i + "] " + anwsers[i].replace("&amp;quot;", "'") + " **\n";
        }

        textmessage = textmessage + "!================!===============!";

        sendMessage(message, textmessage);
    });
}

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items The array containing the items.
 */
function shuffle(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
}

function repbot(message) {
    var number = Math.floor(Math.random() * 5) + 1;
    switch(number){
        case 1:
            sendMessageTTS(message, "It is me, RepBot. Who Dis?");
            break;
        case 2:
            sendMessageTTS(message, "What do you want?");
            break;
        case 3:
            sendMessageTTS(message, "Bro please, calm your tits.");
            break;
        case 4:
            sendMessageTTS(message, "You asked for my assistance?");
            break;
        case 5:
            sendMessageTTS(message, "$conch can you help this pleb?");
            break;
    }
}

function triviaAnswer(message, suffix) {
    let author = message.author.username;

    if (suffix != "") {
        if (question != "") {
            if (correct_answer == anwsers[suffix]) {

                request(apiUrl + 'users/' + author, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        userData = JSON.parse(body);

                        for (var i = 0; i < userData.data.length; i++) {
                            var userdata = userData.data[i];
                            var reps = userdata.reps + 1;
                        }

                        request.put(apiUrl + 'users/').form({
                            "name": author,
                            "reps": reps
                        });
                    }
                });

                // Reset data
                cat = "";
                difficulty = "";
                question = "";
                correct_answer = "";
                incorrect_answers = "";
                type = "";
                anwsers = [];

                sendMessage(message, author + ", you won!!");
            } else {
                request(apiUrl + 'users/' + author, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        userData = JSON.parse(body);

                        for (var i = 0; i < userData.data.length; i++) {
                            var userdata = userData.data[i];
                            var reps = userdata.reps - 1;
                        }

                        request.put(apiUrl + 'users/').form({
                            "name": author,
                            "reps": reps
                        });
                    }
                });
                sendMessage(message, author + ", your answer was incorrect! NOOB");
            }
        } else {
            sendMessage(message, author + ", no trivia is started yet!");
        }
    } else {
        sendMessage(message, author + ", your forgot to give an answer you Faggit!");
    }
}